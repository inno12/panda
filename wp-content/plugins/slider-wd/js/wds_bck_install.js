jQuery(document).ready(function () {

    /*Ajax hide notice forever  */
    jQuery(".wds_bck_notice_dissmiss").on("click", function () {
        var data = {
            'wds_bk_notice_status': '2',
            'action':'backup_install_notice_status',
        };
        jQuery.post(wds_sliders_url, data, function (response) {
            jQuery("#wds_bck_notice_cont").hide();
        });
    })
});

/* Ajax hide popup notice */
function wds_backup_popup_dismiss() {
    var data = {
        'wds_bk_notice_status': '',
    };
    jQuery.post(wds_sliders_url, data, function (response) {
        
    });
}

/* Set option status 2 - never show again during install btn click in popup */
function wds_backup_popup_install() {
    var data = {
        'wds_bk_notice_status': '2',
    };
    jQuery.post(wds_sliders_url, data, function (response) {

    }).done(function() {
        window.location.href = jQuery('#wds_bk_install').attr('href_data');
    });
}

// Set option status 2 - never show again during install btn click in notice
function wds_backup_notice_install() {
    var data = {
        'wds_bk_notice_status': '2',
    };
    jQuery.post(wds_sliders_url, data, function (response) {
    });
}