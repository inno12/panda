<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'panda');

/** MySQL database username */
define('DB_USER', 'panda');

/** MySQL database password */
define('DB_PASSWORD', 'panda123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'QI]q+M2sObme7$^`30f|x9_M|]`19n(goNGP1O=->+~:.t/=X|#`>%YC8V]!qUgg');
define('SECURE_AUTH_KEY',  '-$U ,JiPS~(-``,FjXf9h+=FtOk:W]qqw2l|BmTt~1e] M++_ G&sn|1oX@D)Tq,');
define('LOGGED_IN_KEY',    '/>.43=pfAQx@UqeN]1z?pPG_yE%2,X5RN_+Oz|d>pwH +iAeHqT6-`C({%+`>E~y');
define('NONCE_KEY',        ':oI9< _G!45&o~Y%/<?htn3=dc-03N@#Q42?5Zg/4+,,+XV=K-bxh4#Go?pg~>le');
define('AUTH_SALT',        '(|+YlLBGk]uiX4ui&j-O/<LOMabikE0B=4|P#7vb4@(h`f<uvBNYQg}PL7+hY4F{');
define('SECURE_AUTH_SALT', 'WBC(8GN!},9!|b1N:7^oD6?<-Svd^VoyPHvzhA&vS(<H48jjc6~oN=e@<@.F@:b+');
define('LOGGED_IN_SALT',   'S,TH[4X@EO++?CzK86+r>o+,R<5H:yqv~[&?S~Uz=Bt+)ko*$7sdA^/!PeI7OHur');
define('NONCE_SALT',       '4YbxnL)A/~HkEL/cLO0?cBi!f$4$J-yg2}DK{jBVCMk3@wI6^iOeHkMPZR9O*7$r');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('RELOCATE',true);
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
